<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpressprojectblog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gZaAOs{]t~KZ=~A;t$UsV},i kR_0YtHi{1lLD[Dd+-)O5E*j!+{#Woxq|>bn6C|' );
define( 'SECURE_AUTH_KEY',  'xDc5*SQ^5&BMr_>rdAc`i&7y(-/J qO~%1R%)5^JIf<2C2VRoUyva1Mx][z^Z%wy' );
define( 'LOGGED_IN_KEY',    '<2:G J5mkC2A|iu|DZHUYK^%p&ki~lPBk1YO$?sVkJ?W(NIX._h=V@4Cod`%`1S+' );
define( 'NONCE_KEY',        '*,xCmLN?.1k;!k:mtw`Azkg5U]8#^wQjKV1Pwlby$jD_t{Nul,w#i5QXcBTn`x];' );
define( 'AUTH_SALT',        'O}VHV0c=cU*dqPbgqn*ML8[6&p2>j8$eD!1=uA@t=%QhDi_fE?<bMZ*QX@/`=mn7' );
define( 'SECURE_AUTH_SALT', 'u5g[LsmCK|B6TeZ-?B|]@Te[##2UD*9a[ouF,I}o3yJ+dXnoG$ 2FvnQb=jD@%fb' );
define( 'LOGGED_IN_SALT',   '^5ILc-,bW`Pf@J^|]6Nupse>to8!,BE-Q7 n{>}_|cAGvbl@JZP7O(R8Rw%QthvK' );
define( 'NONCE_SALT',       'ySnQG+JFrL_{Cf-A?|tTXZ:Otj9{AI/;De[P~K;R!)rX~#8`o5pEX@#g:Dpes_mG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
